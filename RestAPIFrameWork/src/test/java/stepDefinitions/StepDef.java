package stepDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.RestAssured; 
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import org.hamcrest.*;
import static org.junit.Assert.*;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;
import java.io.*;


@SuppressWarnings("deprecation")
public class StepDef {


	
	@Given("List user API with prefix is called")
	public void List_user_API_with_prefix_is_called() {
		try {
			FileReader flReader = new FileReader("Resources//Environment.properties");
			Properties envProeprties= new Properties();
			envProeprties.load(flReader);
			System.out.println(envProeprties.getProperty("baseURI"));
		RestAssured.baseURI=envProeprties.getProperty("baseURI");
		String token1= envProeprties.getProperty("TSToken");
		RestAssured.useRelaxedHTTPSValidation();
		Response res1;
		String pre[] = {"RASIKA","virendra","AMruta","alek","Ramesh","sampada","chrisw"};
		int i =0;
		Loop1:
			do {
	
			 res1= given().headers("Authorization", "TSToken "+token1, "Content-Type", "application/json","User-Agent","PostmanRuntime/7.28.4","Accept","*/*","Accept-Encoding","gzip, deflate, br","Connection","keep-alive", "X-TS-Client-Version", "6.0.1; [1,2,3,11,12]", "locale", "")
							.queryParam("prefix",pre[i]).get("api/v2/mng/support/user/actions/partial").then().log().all().extract().response();
					
		
		String responseBody= res1.asString();
		JsonPath jsData = new JsonPath(responseBody);
		List<String> userAcct= jsData.getList("data");
		int lsize= userAcct.size();
		
		if(lsize==0) {
			System.out.println("0 size");
		}
		else {
			//for(int j=0;j<lsize;j++) {			}
			for(String tempUseracct :userAcct) {
				System.out.println(tempUseracct);
				Response res2=given().headers("Authorization", "TSToken "+token1, "Content-Type", "application/json","User-Agent","PostmanRuntime/7.28.4","Accept","*/*","Accept-Encoding","gzip, deflate, br","Connection","keep-alive", "X-TS-Client-Version", "6.3; [1,2,3,11,12]", "locale", "")
					.queryParam("uid",tempUseracct).delete("api/v2/mng/support/user").then().log().all().extract().response();
				System.out.println(res2);
			
			}
		}  
		System.out.println(i);
		if(lsize == 10) {
			continue Loop1;	
		}	
		i++;
		}while(i<pre.length);
		
		}catch(Exception E) {System.out.println(E);}
	}

}
